package com.api.exampla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamplaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamplaApplication.class, args);
	}

}
