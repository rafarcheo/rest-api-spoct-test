package com.api.exampla.controller

import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * @author Keesun Baik
 */
class SampleControllerTest extends Specification {

    MockMvc mockMvc;

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new MainController()).build()
    }

    def "main controller test"() {
        when:
        def response = mockMvc.perform(get("/"))

        then:
        response.andExpect(status().isOk())
                .andExpect(content().string("Hello world!"))

        response.andReturn().response.contentAsString == "Hello world!"
    }
}
